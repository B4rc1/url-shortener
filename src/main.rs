use anyhow::{bail, Context, Result};
use handlebars::Handlebars;
use serde_json::json;
use std::{
    collections::HashMap,
    env,
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
    sync::Arc,
};

fn register_template(handlebars: &mut Handlebars, name: &str) -> Result<()> {
    let fpath = format!("./src/templates/{name}.hbs");
    handlebars
        .register_template_file(name, &fpath)
        .with_context(|| format!("Failed to load template file from {fpath}"))?;

    Ok(())
}

fn setup_handlebars() -> Result<Handlebars<'static>> {
    let mut handlebars = Handlebars::new();
    handlebars.set_dev_mode(true);

    register_template(&mut handlebars, "redirect")?;
    register_template(&mut handlebars, "error")?;

    Ok(handlebars)
}

fn main() -> Result<()> {
    // force rust backtraces
    env::set_var("RUST_BACKTRACE", "1");

    let listener = TcpListener::bind("127.0.0.1:1337").unwrap();

    let handlebars = Arc::new(setup_handlebars()?);

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        let res = handle_connection(stream, &handlebars);
        match res {
            Ok(_) => {}
            Err(e) => {
                println!("Unhandled error while handeling connection:{:#?}", e);
            }
        }
    }

    Ok(())
}
fn handle_connection(mut stream: TcpStream, handlebars: &Handlebars) -> Result<()> {
    let urls: HashMap<String, String> = HashMap::from(
        [
            ("/nixos", "https://nixos.org/"),
            ("/google", "https://google.com/"),
        ]
        .map(|(l, t)| (l.to_string(), t.to_string())),
    );

    let buf_reader = BufReader::new(&mut stream);
    let http_request: Vec<_> = buf_reader
        .lines()
        .map(|result| result.unwrap())
        .take_while(|line| !line.is_empty())
        .collect();

    let response = match process_request(http_request, &handlebars, &urls) {
        Ok(r) => r,
        Err(e) => {
            let data = json!({
                "error": serde_json::to_value(format!("{:?}", e).split("\n").collect::<Vec<_>>())?,
            });

            let rendered_page = handlebars.render("error", &data)?;
            let length = rendered_page.len();

            format!("HTTP/1.1 500 Internal Server Error\r\nContent-Lenght: {length}\r\n\r\n{rendered_page}")
        }
    };

    stream.write_all(response.as_bytes()).unwrap();

    Ok(())
}

fn process_request(
    req: Vec<String>,
    handlebars: &Handlebars,
    urls: &HashMap<String, String>,
) -> Result<String> {
    let request_line: Vec<_> = req
        .first()
        .expect("empty http request")
        .split(" ")
        .collect();

    let (req_type, req_path, req_protocol) = (request_line[0], request_line[1], request_line[2]);

    if !(req_type == "GET" && req_protocol == "HTTP/1.1") {
        bail!("unhandled request, {:?}", request_line);
    }

    println!("Request: {:#?}", req_path);

    // let _ = "NaN".parse::<u32>().context("Artifical error")?;

    let response: String;

    if let Some(url) = urls.get(req_path) {
        let data = &json!({ "url": url });

        let rendered_page = handlebars.render("redirect", data)?;
        let length = rendered_page.len();

        response = format!("HTTP/1.1 200 OK\r\nContent-Lenght: {length}\r\n\r\n{rendered_page}");
    } else {
        response = "HTTP/1.1 404 Not Found\r\n\r\n404 no file found :(".to_string();
    }

    Ok(response)
}
